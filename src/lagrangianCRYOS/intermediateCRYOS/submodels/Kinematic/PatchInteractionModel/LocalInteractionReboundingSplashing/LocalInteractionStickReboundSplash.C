/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2014 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "LocalInteractionStickReboundSplash.H"
#include "mathematicalConstants.H" //J added
#include "meshTools.H" //J added

// * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * * //

template<class CloudType>
Foam::LocalInteractionStickReboundSplash<CloudType>::LocalInteractionStickReboundSplash
(
    const dictionary& dict,
    CloudType& cloud
)
:
    PatchInteractionModel<CloudType>(dict, cloud, typeName),
    patchData_(cloud.mesh(), this->coeffDict()),      
    nEscape_(patchData_.size(), 0),
    massEscape_(patchData_.size(), 0.0),
    nStick_(patchData_.size(), 0),
    massStick_(patchData_.size(), 0.0),
    writeFields_(this->coeffDict().lookupOrDefault("writeFields", true)),
    massEscapePtr_(NULL),
    massStickPtr_(NULL),
    sizeDistributionTriple_
    (
        distributionModelsTriple::distributionModelTriple::New
        (
            this->coeffDict().subDict("sizeDistributionTriple"),
            this->owner().rndGen() 
        )
    )
{
    if (writeFields_)
    {
        word massEscapeName(this->owner().name() + ":massEscape");
        word massStickName(this->owner().name() + ":massStick");
        
        Info<< "    Interaction fields will be written to " << massEscapeName << "," 
		<< " and " << massStickName << endl;

        (void)massEscape();
        (void)massStick();

    }
    else
    {
        Info<< "    Interaction fields will not be written" << endl;
    }

    // check that interactions are valid/specified
    forAll(patchData_, patchI)
    {
        const word& interactionTypeName =
            patchData_[patchI].interactionTypeName();
        const typename PatchInteractionModel<CloudType>::interactionType& it =
            this->wordToInteractionType(interactionTypeName);

        if (it == PatchInteractionModel<CloudType>::itOther)
        {
            const word& patchName = patchData_[patchI].patchName();
            FatalErrorIn("LocalInteractionStickReboundSplash(const dictionary&, CloudType&)")
                << "Unknown patch interaction type "
                << interactionTypeName << " for patch " << patchName
                << ". Valid selections are:"
                << this->PatchInteractionModel<CloudType>::interactionTypeNames_
                << nl << exit(FatalError);
        }
    }
}


template<class CloudType>
Foam::LocalInteractionStickReboundSplash<CloudType>::LocalInteractionStickReboundSplash
(
    const LocalInteractionStickReboundSplash<CloudType>& pim
)
:
    PatchInteractionModel<CloudType>(pim),
    patchData_(pim.patchData_),
    nEscape_(pim.nEscape_),
    massEscape_(pim.massEscape_),
    nStick_(pim.nStick_),
    massStick_(pim.massStick_),
    writeFields_(pim.writeFields_),
    massEscapePtr_(NULL),
    massStickPtr_(NULL),
    sizeDistributionTriple_(pim.sizeDistributionTriple_().clone().ptr())

{}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

template<class CloudType>
Foam::LocalInteractionStickReboundSplash<CloudType>::~LocalInteractionStickReboundSplash()
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

template<class CloudType>
Foam::volScalarField& Foam::LocalInteractionStickReboundSplash<CloudType>::massEscape()
{
    if (!massEscapePtr_.valid())
    {
        const fvMesh& mesh = this->owner().mesh();

        massEscapePtr_.reset
        (
            new volScalarField
            (
                IOobject
                (
                    this->owner().name() + ":massEscape",
                    mesh.time().timeName(),
                    mesh,
                    IOobject::READ_IF_PRESENT,
                    IOobject::AUTO_WRITE
                ),
                mesh,
                dimensionedScalar("zero", dimMass, 0.0)
            )
        );
    }

    return massEscapePtr_();
}


template<class CloudType>
Foam::volScalarField& Foam::LocalInteractionStickReboundSplash<CloudType>::massStick()
{
    if (!massStickPtr_.valid())
    {
        const fvMesh& mesh = this->owner().mesh();

        massStickPtr_.reset
        (
            new volScalarField
            (
                IOobject
                (
                    this->owner().name() + ":massStick",
                    mesh.time().timeName(),
                    mesh,
                    IOobject::READ_IF_PRESENT,
                    IOobject::AUTO_WRITE
                ),
                mesh,
                dimensionedScalar("zero", dimMass, 0.0)
            )
        );
    }

    return massStickPtr_();
}

template<class CloudType>
bool Foam::LocalInteractionStickReboundSplash<CloudType>::correct
(
    typename CloudType::parcelType& p,
    const polyPatch& pp,
    bool& keepParticle,
    const scalar trackFraction,
    const tetIndices& tetIs
)
{	
    label patchI = patchData_.applyToPatch(pp.index());

    if (patchI >= 0)
    {
        vector& U = p.U();
        bool& active = p.active();

        typename PatchInteractionModel<CloudType>::interactionType it =
            this->wordToInteractionType
            (
                patchData_[patchI].interactionTypeName()
            );

        switch (it)
        {
            case PatchInteractionModel<CloudType>::itEscape:
            {
                scalar dm = p.mass()*p.nParticle();

                keepParticle = false;
                active = false;
                U = vector::zero;
                nEscape_[patchI]++;
                massEscape_[patchI] += dm;
                if (writeFields_)
                {
                    label pI = pp.index();
                    label fI = pp.whichFace(p.face());
                    massEscape().boundaryFieldRef()[pI][fI] += dm;
                }
                break;
            }
            case PatchInteractionModel<CloudType>::itStick:
            {
                scalar dm = p.mass()*p.nParticle();

                keepParticle = true;
                active = false;
                U = vector::zero;
                nStick_[patchI]++;
                massStick_[patchI] += dm;
                if (writeFields_)
                {
                    label pI = pp.index();
                    label fI = pp.whichFace(p.face());
                    massStick().boundaryFieldRef()[pI][fI] += dm;
                }
                break;
            }
            case PatchInteractionModel<CloudType>::itRebound:
            {
                keepParticle = true;
                active = true;

                vector nw;
                vector Up;

                this->owner().patchData(p, pp, nw, Up);


                // Calculate motion relative to patch velocity
                U -= Up;

                scalar Un = U & nw;
                vector Ut = U - Un*nw;

                if (Un > 0)
                {
                    U -= (1.0 + patchData_[patchI].e())*Un*nw;
                }

                U -= patchData_[patchI].mu()*Ut;

                // Return velocity to global space
                U += Up;

                break;
            }
            case PatchInteractionModel<CloudType>::itStickRebound: //O added. For non-snow surfaces: only stick and rebound, without splash.
            {
                vector nw;
                vector Up;
                this->owner().patchData(p, pp, nw, Up); 

				const fvMesh& mesh = this->owner().mesh(); 
				cachedRandom& ranGen = this->owner().rndGen(); 
				label pI = pp.index(); 
                label fI = pp.whichFace(p.face()); 
                scalar cellArea = mesh.magSf().boundaryField()[pI][fI]; 
                label cellInd = mesh.faceOwner()[fI];               

                // Calculate motion relative to patch velocity
                U -= Up;
                
                vector n = -nw; // n is inward... in impacting plane
                vector Un  = (U & n)*n;
                
                vector Ut1  = U - Un; //... in the impacting plane
                vector t1 = Ut1/(mag(Ut1)+ROOTVSMALL); 
                
                vector t2 = t1^n; //... normal to impacting plane
                
               	// IMPACT PROPERTIES
                scalar i_vel = mag(U);  
                scalar n_impact = p.nParticle(); //number of particles in the parcel
                scalar pMassParcel=p.nParticle()*p.mass();
                scalar i_ene=0.5*pMassParcel*pow(i_vel,2); //pMassParcel represents the mass of the PARCEL
                scalar i_mom=pMassParcel*i_vel;

                scalar i_ang1 = atan(mag(Un)/ (mag(Ut1)+ROOTVSMALL) ); // impacting angle with respect to bed surface, or vang of impacting particle
                scalar i_ang2 = atan(mag(Ut1.y())/ (mag(Ut1.x())+ROOTVSMALL) ); // this is hang of impacting particle with respect to the global reference frame
																 // Angle of projected impacting velocity on the bed surface with respect to x-axis of the global
																 // reference frame. Hence, the hang calculated for splashing is also absolute not respect to impacting frame.
																 // if we want hang with respect to impacting plane, we can set i_ang2=0 to give use hang with respect to impacting angle
				      
				scalar slope = 0.0;                                              
										 
				i_ang2=0.0;	//See explanation by MJ above
				
				 // REBOUND
				scalar prob_reb= 0.9*(1.0-exp(-2.0*i_vel));
				scalar rand = ranGen.sample01<scalar>();
				if(rand<prob_reb && (U & n)<=0. )
				{
					scalar r_vel=0.5*i_vel;
					scalar mean= 45.0/180*constant::mathematical::pi;
					scalar v_ang = sizeDistributionTriple_->exponentialSample(mean,0.0);
					v_ang = min((constant::mathematical::piByTwo-(10e-1)), max((-constant::mathematical::piByTwo+(10e-1)), v_ang+slope));
					Un = (r_vel*sin(v_ang))*n;
					Ut1 = (r_vel*cos(v_ang))*t1;  // O added 
					U = Ut1+Un;
				    // Return velocity to global space
                    U += Up; 
                    keepParticle = true;
				    active = true;				
				}
				else
				{
					this->owner().massDeposition().boundaryFieldRef()[pI][fI] += (pMassParcel)/cellArea; //O added
					this->owner().massCheckPatterns().boundaryFieldRef()[pI][fI] += (pMassParcel)/cellArea; //O added
					keepParticle = false;
					active = false;
					U = vector::zero;					
				}
                break;
            }            
            case PatchInteractionModel<CloudType>::itStickReboundSplash: 
            {
				vector nw;
                vector Up;
                this->owner().patchData(p, pp, nw, Up); 

                const fvMesh& mesh = this->owner().mesh(); // J added
                cachedRandom& ranGen = this->owner().rndGen(); // J added
                label pI = pp.index(); //O added
                label fI = pp.whichFace(p.face()); //O added
                scalar cellArea = mesh.magSf().boundaryField()[pI][fI]; //O added
                label cellInd = mesh.faceOwner()[fI];               

                // Calculate motion relative to patch velocity
                U -= Up;               

                vector n = -nw;// n is inward... in impacting plane
                vector Un  = (U & n)*n;
                
                vector Ut1  = U - Un; //... in the impacting plane
                vector t1 = Ut1/(mag(Ut1)+ROOTVSMALL); 
                
                vector t2 = t1^n; //... normal to impacting plane
                                              	
               	// IMPACT PROPERTIES
                scalar i_vel = mag(U);  
                scalar n_impact = p.nParticle(); //number of particles in the parcel
                scalar pMassParcel=p.nParticle()*p.mass();//(p.rho()*constant::mathematical::pi*pow(p.d(),3)/6.0);
                scalar i_ene=0.5*pMassParcel*pow(i_vel,2); //pMassParcel represents the mass of the PARCEL
                scalar i_mom=pMassParcel*i_vel;
                
                scalar i_ang1 = atan(mag(Un)/ (mag(Ut1)+ROOTVSMALL) ); // impacting angle with respect to bed surface, or vang of impacting particle
                scalar i_ang2 = atan(mag(Ut1.y())/ (mag(Ut1.x())+ROOTVSMALL) ); // this is hang of impacting particle with respect to the global reference frame
																 // Angle of projected impacting velocity on the bed surface with respect to x-axis of the global
																 // reference frame. Hence, the hang calculated for splashing is also absolute not respect to impacting frame.
																 // if we want hang with respect to impacting plane, we can set i_ang2=0 to give use hang with respect to impacting angle
				      
                scalar slope = 0.0;                                              			
									 
                i_ang2=0.0;	//See explanation by MJ above
				
                // REBOUND
                scalar prob_reb= 0.9*(1.0-exp(-2.0*i_vel));
                scalar rand = ranGen.sample01<scalar>();
                if(rand<prob_reb && (U & n)<=0. )
                {
        		scalar r_vel=0.5*i_vel;
        		scalar mean= 45.0/180*constant::mathematical::pi;
        		scalar v_ang = sizeDistributionTriple_->exponentialSample(mean,0.0);
        		v_ang = min((constant::mathematical::piByTwo-(10e-3)), max((-constant::mathematical::piByTwo+(10e-3)), v_ang+slope));
        		Un = (r_vel*sin(v_ang))*n;
        		Ut1 = (r_vel*cos(v_ang))*t1;  // O added 
        		U = Ut1+Un;
        		// Return velocity to global space
        		U += Up; 
        		keepParticle = true;
        		active = true;					
                }
                else
                {
        		this->owner().massDeposition().boundaryFieldRef()[pI][fI] += (pMassParcel)/cellArea; //O added
        		this->owner().massCheckPatterns().boundaryFieldRef()[pI][fI] += (pMassParcel)/cellArea; //O added
        		keepParticle = false;
        		active = false;
        		U = vector::zero;					
                }
				
                // SPLASHING				
				scalar epsilonf_= 0.96*(1.0-prob_reb*patchData_[patchI].epsilonr());
				scalar av_d3= pow(patchData_[patchI].dm()+pow(patchData_[patchI].ds(),2.0)/patchData_[patchI].dm(),3.0);
				scalar sd_d3 = av_d3*sqrt(pow(1.0+pow(patchData_[patchI].ds()/patchData_[patchI].dm(),2.0),9.0)-1.0);
				scalar av_vel = 0.25*pow(i_vel,0.3);
				scalar av_vel2 = 2.0*pow(av_vel,2.0);
				scalar av_mass = p.rho()*constant::mathematical::pi/6.0*av_d3;
				scalar sd_vel  = av_vel;
				scalar sd_vel2 = 2.0*sqrt(5.0)*pow(av_vel,2.0);
				scalar sd_mass = p.rho()*constant::mathematical::pi/6.0*sd_d3;
				
				
				//Check conditions with Daniela
				scalar cos_a = 0.75;
				scalar cos_b  = 0.96;
				scalar cos_i = cos(i_ang1);
				
				scalar av_massvel = av_mass*av_vel*cos_a*cos_b + patchData_[patchI].corrm()*sd_mass*sd_vel;
				scalar av_massvel2 = av_mass*av_vel2 + patchData_[patchI].corre()*sd_mass*sd_vel2;
				scalar n_splash1 = i_ene*(1.0-prob_reb*patchData_[patchI].epsilonr() - epsilonf_)/(0.5*av_massvel2+patchData_[patchI].bEne()+ROOTVSMALL);
				scalar n_splash2 = i_mom*cos_i*(1.0 - prob_reb*patchData_[patchI].mur() - patchData_[patchI].muf())/(av_massvel+ROOTVSMALL);
				scalar n_splash = min(n_splash1,n_splash2); //Number of particles to inject in the model, corresponds to splashed particles.
				
				//SAMPLING NEW EJECTED PARTICLES
				if(n_splash>=n_impact) //If number of splashing particles is greater or equal than number of impacting particles (model choice)
				{
					label np1 = label(n_splash/patchData_[patchI].pppMax())+1;  //This is because last parcel won't be filled up to maximum.
					for(label ip1=1; ip1<=np1; ip1++)
					{
						scalar d_g = sizeDistributionTriple_->logNormalSample(patchData_[patchI].dm(),patchData_[patchI].ds());  //diameter of particle randomly sampled
						d_g = min(patchData_[patchI].d_max(),max(d_g,patchData_[patchI].d_min()));
						scalar mass_g = p.rho()*constant::mathematical::pi*pow(d_g,3)/6.0; //mass_g is the mass of particle, p.rho() is the density of PARTICLE
               
                        
                        scalar dep_mass = (this->owner().massDeposition().boundaryField()[pI][fI])*cellArea; //O added
                        scalar temp_mass = 0.0;
                        
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
						if(ip1!=np1) //If the number of parcels is not the last one, which might not be completely full
						{
							temp_mass = patchData_[patchI].pppMax()*mass_g; //O added
						}
						else  //If it is the last parcel which is not having 5000 particles
						{
							temp_mass = (n_splash-(np1-1)*patchData_[patchI].pppMax())*mass_g; //O added
						}
						
						if(temp_mass > dep_mass) //O added
						{
							temp_mass = dep_mass; //O added
						}
                       
				        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
						if(temp_mass > 0.0) //O added
						{

							vector coorC = mesh.C()[p.cell()];

							parcelType* pPtr = new parcelType(mesh,coorC,p.cell());								
							
							//Check/set new parcel thermo properties
							this->owner().setParcelThermoProperties(*pPtr, 0.0);
							
							pPtr->d()=d_g; //assigning the diameter
							
							pPtr->nParticle()=temp_mass/mass_g; //O added
							
							//Check/set new parcel injection properties
							this->owner().checkParcelProperties(*pPtr, 0.0*mesh.time().deltaTValue(), false);// 	p.stepFraction()=0;	J com
											 
							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
							scalar e_vel=sizeDistributionTriple_->exponentialSample(av_vel,0.0);  //ejection velocity sampled from distribution
							scalar v_ang=sizeDistributionTriple_->exponentialSample(50.0/180.0*constant::mathematical::pi,0.0);
							scalar h_ang=sizeDistributionTriple_->normalSample(i_ang2,15.0/180.0*constant::mathematical::pi);
							h_ang   = min(constant::mathematical::pi, max(-constant::mathematical::pi, h_ang));
							v_ang = min((constant::mathematical::piByTwo-(10e-3)), max((-constant::mathematical::piByTwo+(10e-3)), v_ang+slope));

							vector Un_splashing = (e_vel*sin(v_ang))*n;
							vector Ut1_splashing = (e_vel*cos(v_ang)*cos(h_ang))*t1;
							vector Ut2_splashing = (e_vel*cos(v_ang)*sin(h_ang))*t2;
							vector Ut_splashing = Un_splashing+Ut1_splashing+Ut2_splashing;
							// Return velocity to global space
							Ut_splashing += Up; 
			
							pPtr->U()=Ut_splashing; //assigning the splashing linear velocity 

							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
												
							// Apply corrections to position for 2-D cases
							point pos=pPtr->position();
							meshTools::constrainToMeshCentre(mesh, pos);
							
							// Apply correction to velocity for 2-D cases
							meshTools::constrainDirection
							(
								mesh,
								mesh.solutionD(),
								pPtr->U()
							);
							this->owner().addParticle(pPtr);	
							this->owner().massDeposition().boundaryFieldRef()[pI][fI] -= temp_mass/cellArea; //O added
							this->owner().massCheckPatterns().boundaryFieldRef()[pI][fI] -= temp_mass/cellArea; //O added											    
					    }
												
				    }
				}

                break;				
			}
            default:
            {
                FatalErrorIn
                (
                    "bool LocalInteractionStickReboundSplash<CloudType>::correct"
                    "("
                        "typename CloudType::parcelType&, "
                        "const polyPatch&, "
                        "bool&, "
                        "const scalar, "
                        "const tetIndices&"
                    ") const"
                )   << "Unknown interaction type "
                    << patchData_[patchI].interactionTypeName()
                    << "(" << it << ") for patch "
                    << patchData_[patchI].patchName()
                    << ". Valid selections are:" << this->interactionTypeNames_
                    << endl << abort(FatalError);
            }
        }

        return true;
    }

    return false;
}


template<class CloudType>
void Foam::LocalInteractionStickReboundSplash<CloudType>::info(Ostream& os)
{
    // retrieve any stored data
    labelList npe0(patchData_.size(), 0);
    this->getModelProperty("nEscape", npe0);

    scalarList mpe0(patchData_.size(), 0.0);
    this->getModelProperty("massEscape", mpe0);

    labelList nps0(patchData_.size(), 0);
    this->getModelProperty("nStick", nps0);

    scalarList mps0(patchData_.size(), 0.0);
    this->getModelProperty("massStick", mps0);
   
    // accumulate current data
    labelList npe(nEscape_);
    Pstream::listCombineGather(npe, plusEqOp<label>());
    npe = npe + npe0;

    scalarList mpe(massEscape_);
    Pstream::listCombineGather(mpe, plusEqOp<scalar>());
    mpe = mpe + mpe0;

    labelList nps(nStick_);
    Pstream::listCombineGather(nps, plusEqOp<label>());
    nps = nps + nps0;

    scalarList mps(massStick_);
    Pstream::listCombineGather(mps, plusEqOp<scalar>());
    mps = mps + mps0;
    
    forAll(patchData_, i)
    {
        os  << "    Parcel fate (number, mass)      : patch "
            <<  patchData_[i].patchName() << nl
            << "      - escape                      = " << npe[i]
            << ", " << mpe[i] << nl
            << "      - stick                       = " << nps[i]
            << ", " << mps[i] << nl;
    }

    if (this->writeTime())
    {
        this->setModelProperty("nEscape", npe);
        nEscape_ = 0;

        this->setModelProperty("massEscape", mpe);
        massEscape_ = 0.0;

        this->setModelProperty("nStick", nps);
        nStick_ = 0;

        this->setModelProperty("massStick", mps);
        massStick_ = 0.0;
    }
}
// ************************************************************************* //
