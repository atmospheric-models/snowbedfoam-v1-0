/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::distributionModelTriple

Description
    A library of runtime-selectable distribution models.

    Returns a sampled value given the expectation (nu) and variance (sigma^2)

    Current distribution models include:
    - exponential
    - fixedValue
    - general
    - multi-normal
    - normal
    - Rosin-Rammler
    - uniform

    The distributionModelTriple is tabulated in equidistant nPoints, in an interval.
    These values are integrated to obtain the cumulated distribution model,
    which is then used to change the distribution from unifrom to
    the actual distributionModelTriple.

SourceFiles
    distributionModelTriple.C
    distributionModelTriple.C

\*---------------------------------------------------------------------------*/

#ifndef distributionModelTriple_H
#define distributionModelTriple_H

#include "IOdictionary.H"
#include "autoPtr.H"
#include "cachedRandom.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace distributionModelsTriple
{

/*---------------------------------------------------------------------------*\
                     Class distributionModelTriple Declaration
\*---------------------------------------------------------------------------*/

class distributionModelTriple
{

protected:

    // Protected data

        //- Coefficients dictionary
        const dictionary distributionModelTripleDict_;

        //- Reference to the random number generator
        cachedRandom& rndGen_;


    // Protected Member Functions

        //- Check that the distribution model is valid
        virtual void check() const;


public:

    //-Runtime type information
    TypeName("distributionModelTriple");


    //- Declare runtime constructor selection table
    declareRunTimeSelectionTable
    (
        autoPtr,
        distributionModelTriple,
        dictionary,
        (
            const dictionary& dict,
            cachedRandom& rndGen
        ),
        (dict, rndGen)
    );


    // Constructors

        //- Construct from dictionary
        distributionModelTriple
        (
            const word& name,
            const dictionary& dict,
            cachedRandom& rndGen
        );

        //- Construct copy
        distributionModelTriple(const distributionModelTriple& p);

        //- Construct and return a clone
        virtual autoPtr<distributionModelTriple> clone() const
        {
            return autoPtr<distributionModelTriple>(new distributionModelTriple(*this));
        }


    //- Selector
    static autoPtr<distributionModelTriple> New
    (
        const dictionary& dict,
        cachedRandom& rndGen
    );


    //- Destructor
    virtual ~distributionModelTriple();


    // Member Functions

        //- Sample the normal distribution model
        virtual scalar normalSample(scalar mean_, scalar std_) const;
        
        //- Sample the lognormal distribution model
        virtual scalar logNormalSample(scalar mean_, scalar std_) const;
        
        //- Sample the exponential distribution model
        virtual scalar exponentialSample(scalar mean_, scalar std_) const;

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace distributionModelsNew
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
