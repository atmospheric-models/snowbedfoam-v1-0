word counter_str = Foam::name(counter);
scalar Ustar_ = readScalar(timeSeries.lookup("Ustar"+counter_str));
Info << "Reading Ustar" << counter_str << ", the friction velocity for time period " << counter_str << ": " << Ustar_ << " m/s \n" << endl;

scalar H_ = readScalar(transportProperties.lookup("H"));
Info << "Reading H_, the height for the fluid domain  " << H_ << "\n" << endl;

vector flowDirection_(timeSeries.lookup("flowDirection"+counter_str));
Info << "Reading flowDirection"<< counter_str <<", the flow direction for time period " << counter_str << ": " << flowDirection_ << "\n" << endl;

bool constantPGrad_(transportProperties.lookupOrDefault<bool>("constantPGrad", false));
Info << "Reading the flag if constant pressure gradients is applied to momentum , " << constantPGrad_ << "\n" << endl;

vector dP_dx = (constantPGrad_) ? (Foam::pow(Ustar_,2.0)/H_)*flowDirection_ : vector::zero;
Info << "Updating the pressure gradient in the flow direction for time period " << counter_str << ": " << dP_dx << "\n" << endl;

const pointField& ctrs = mesh.cellCentres();
forAll(ctrs, cellI)
{
 gradP[cellI]=dP_dx;	
}

gradP.correctBoundaryConditions();

